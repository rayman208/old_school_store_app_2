package com.example.old_school_store_app.views.main;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.old_school_store_app.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}